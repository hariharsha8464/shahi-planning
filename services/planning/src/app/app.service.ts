import { Injectable } from '@nestjs/common';
import { projectPlanningDataSource } from './app-data-source';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }
}
