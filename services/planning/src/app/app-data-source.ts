import { appConfig } from "../../config";
import { DataSource } from "typeorm";
import { UserEntity } from "./user/entity/user.entity";
import { DesignationEntity } from "./masters/designation/entity/designation.entity";
import { EmployeeEntity } from "./masters/employees/entity/employee.entity";
import { RoleEntity } from "./masters/role/entity/role.entity";
import { DepartmentEntity } from "./masters/department/entity/department.entity";
import { UnitEntity } from "./masters/branch/entity/unit.entity";
import { LayPlanSchedulesEntity } from "./planning/entities/Lay-plan-data.entity";
import { GtsManpowerEntity } from "./planning/entities/manpower-entity";

export const projectPlanningDataSource = new DataSource({
    type: "mssql",
    host: "172.25.23.4",
    username: "sa",
    password: "manager@123",
    database: "Planning",
    // name:'Planning',
    synchronize: false,
    logging: true,
    // requestTimeout:90000,
    extra: {
        validateConnection: true,
        trustServerCertificate: true,
    },
    options: {
        cryptoCredentialsDetails: {
            minVersion: "TLSv1",
        },
    },
    entities: [
        DepartmentEntity,UserEntity,EmployeeEntity,DesignationEntity,UnitEntity,LayPlanSchedulesEntity
    ],
})
export const projectPlanningDataSource23 = new DataSource({
    type: "mssql",
    host: "172.25.23.4",
    username: "sa",
    password: "manager@123",
    database: "SEPL_Scan",
    synchronize: false,
    logging: true,
    requestTimeout:209999,
    extra: {
        validateConnection: true,
        trustServerCertificate: true,
    },
    options: {
        cryptoCredentialsDetails: {
            minVersion: "TLSv1",
        },
    }
})
export const projectPlanningDataSource01 = new DataSource({
    type: "mssql",
    host: "172.25.1.91",
    username: "sa",
    password: "Shahimssql2019",
    database: "SEPL_Scan",
    synchronize: false,
    extra: {
        validateConnection: true,
        trustServerCertificate: true,
    },
    options: {
        cryptoCredentialsDetails: {
            minVersion: "TLSv1",
        },
    }
})
export const projectPlanningDataSource3 = new DataSource({
    type: "mssql",
    host: "172.25.23.4",
    username: "sa",
    password: "manager@123",
    database: "GTS_Libraries_Live",
    synchronize: false,
    logging: true,
    extra: {
        validateConnection: true,
        trustServerCertificate: true,
    },
    options: {
        cryptoCredentialsDetails: {
            minVersion: "TLSv1",
        },
    },
    entities: [ GtsManpowerEntity,LayPlanSchedulesEntity]
})
export const projectPlanningDataSource54 = new DataSource({
    type: "mssql",
    host: "172.25.54.194",
    username: "gts.unit54",
    password: "gts.unit54",
    database: "SEPL_Scan",
    synchronize: false,
    logging: true,
    extra: {
        validateConnection: true,
        trustServerCertificate: true,
    },
    options: {
        cryptoCredentialsDetails: {
            minVersion: "TLSv1",
        },
    }
})

export const AppDataSource = new DataSource({
    type: "mysql",
    host: '172.20.50.169',
    username: 'internal_apps',
    password: 'Schemax@2023',
    database: 'internal_apps',
    synchronize: false,
    logging:true,
    entities:[DepartmentEntity,UserEntity,EmployeeEntity,DesignationEntity,UnitEntity,LayPlanSchedulesEntity,GtsManpowerEntity]
   
})

