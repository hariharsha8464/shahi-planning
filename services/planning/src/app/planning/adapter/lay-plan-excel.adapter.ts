import { Injectable } from '@nestjs/common';
import { LayPlanExcelModel } from '../model/lay-plan-excel.model';
import { LayPlanSchedulesEntity } from '../entities/Lay-plan-data.entity';

@Injectable()
export class LayPlanExcelAdapter {

    public convertDtoToEntity(dto: LayPlanExcelModel): LayPlanSchedulesEntity {
        console.log(dto,'adapter')
        const entity = new LayPlanSchedulesEntity();
        entity.unit = dto.UNIT;
        entity.operation = dto.OPERATION;
        entity.line = dto.LINE;
        entity.scheduleNo = dto.SCHEDULE_NO;
        // entity.status = dto.status;
        entity.created_at = dto.created_at;
        console.log(entity)
        return entity
    }

    public covertEntityToDto(obj: LayPlanSchedulesEntity): LayPlanExcelModel {
       
        const dto = new LayPlanExcelModel(obj.unit,obj.operation,obj.line,obj.scheduleNo,obj.created_at);
        return dto;
    }
}