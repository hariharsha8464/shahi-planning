import { ApiProperty } from "@nestjs/swagger";

export class LayPlanExcelModel {

    @ApiProperty()
    UNIT:string;
    OPERATION:string;
    LINE:string;
    SCHEDULE_NO: string;
    created_at:Date;
    constructor(UNIT:string,OPERATION:string,LINE:string,SCHEDULE_NO:string,created_at:Date){
        this.UNIT = UNIT;
        this.OPERATION = OPERATION;
        this.LINE = LINE;
        this.SCHEDULE_NO = SCHEDULE_NO;
        created_at = created_at;
    }
}