import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { PlanningService } from './planning.service';
import { CommonResponse } from 'libs/libs/shared-models/src/common';
import { ManPowerDto, UnitReq } from 'libs/shared-models';

@Controller('planning')
export class PlanningController {

    constructor(private readonly planningService:PlanningService){}


    @Post('/saveLayPlanExcelData')
    async saveLayPlanExcelData(@Body() formData: any): Promise<CommonResponse>{
        try{
            return this.planningService.saveLayPlanExcelData(formData)
        }catch(error){
            error
        }
    }
    @Get('/getAllPlanData')
    async getAllPlanData():Promise<CommonResponse>{
        try{
            return this.planningService.getAllPlanData()
        }catch(error){
            error
        }
    }

    @Post('/getOperationData')
    async getOperationData(@Body()Req:any):Promise<CommonResponse>{
        try{
            return this.planningService.getOperationData(Req)
        }catch(error){
            error
        }
    }

    @Post('/getOperationsForDrop')
    async getOperationsForDrop(@Body() req: any): Promise<CommonResponse> {
      try {
        console.log(req, 'controller unit req');
        const result = await this.planningService.getOperationsForDrop(req);
        console.log('Result:', result);
        return result;
      } catch (error) {
        console.error(error);
        throw new Error('Error occurred in the controller');
      }
    }
    @Post('/manpowerUpdate')
    async manpowerUpdate(@Body() item: any): Promise<CommonResponse>{
        try{
            return this.planningService.manpowerUpdate(item)
        }catch(error){
            error
            console.log(error)
        }
    }

    @Post('/getAllManpowerData')
    async getAllManpowerData(@Body() req:any):Promise<CommonResponse>{
      try {
        return this.planningService.getAllManpowerData(req);
        
      } catch (error) {
        error;
      }
    }

    @Get('/getUnitsForDropdown')
    async getUnitsForDropdown():Promise<CommonResponse>{
      try {
        return this.planningService.getUnitsForDropdown();
        
      } catch (error) {
        error;
      }
    }

    @Post('/getWorkStationsDrop')
    async getWorkStationsDrop(@Body() Req: any): Promise<CommonResponse> {
      try {
        console.log('controller', Req);
        const result = await this.planningService.getWorkStationsDrop(Req);
        return result;
      } catch (error) {
        console.error('Error:', error);
      }
    }
    
    @Post('/getAllLineNumbersForDropdown')
    async getAllLineNumbersForDropdown(@Body() Req:any):Promise<CommonResponse>{
      try {
        console.log('controller', Req);
        const result = await this.planningService.getAllLineNumbersForDropdown(Req);
        return result;
      } catch (error) {
        console.error('Error:', error);
      }
    }


}
