import { Module } from '@nestjs/common';
import { PlanningService } from './planning.service';
import { PlanningController } from './planning.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LayPlanExcelAdapter } from './adapter/lay-plan-excel.adapter';

@Module({
  imports:[
    TypeOrmModule.forFeature([

        ]),
    
  ],
  controllers:[PlanningController],
  providers: [PlanningService,LayPlanExcelAdapter],
  exports : [PlanningService]
})
export class PlanningModule { }
