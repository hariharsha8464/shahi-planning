import { Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('manpower_allocation')
export class GtsManpowerEntity {
    @PrimaryGeneratedColumn("increment",{
        name:'id',
    })
    id: number;

    @Column({
        name:'unit',
        type:'int',
    })
    unit: number;

    @Column({
        name:'shift',
        type:'varchar',
    })
    shift: string;

    @Column({
        name:'work_station',
        type:'varchar',
    })
    workStation: string;

    @Column({
        name:'line_number',
        type:'varchar',
    })
    lineNumber: string;

    @Column({
        name:'machine_number',
        type:'varchar',   })
    machineNumber: string;
    
    @Column({
        name:'accepted_pcs',
        type:'int',
    })
    acceptedPcs: number; 
    
    @Column({
        name:'pc_rate',
        type:'int',
    })
    pcRate: number;

    @Column({
        name:'rejected_pcs',
        type:'int',
    })
    rejectedPcs: number;

    @Column({
        name:'operators',
        type:'int',
    })
    operators: number;  


    @Column({
        name:'kb_operators',
        type:'int',   })
    kbOperators: number;


    @Column({
        name:'pos_rate_operators',
        type:'int',
    })
    posRateOperators: number;

    @Column({
        name:'ot_operators',
        type:'int',
    })
    otOperators: number;

    @Column({
        name:'hours',
        type:'int',
    })
    hours: number;


    @Column({
        name:'rpdt',
        type:'int',
    })
    RPDT: number;

    @CreateDateColumn({
        default: () => 'CURRENT_TIMESTAMP',
        name:'created_at',
    })
    createdAt:Date;

    @Column({
        name:'created_user',
        type:'varchar',
    })
    createdUser: string;

}