import { Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('Lay_Plan_Schedules')
export class LayPlanSchedulesEntity {
    @PrimaryGeneratedColumn("increment",{
        name:'id',
        type:'int'
    })
    id: number;

    @Column({
        name:'UNIT',
        type:'varchar',
    })
    unit: string;

    @Column({
        name:'OPERATION',
        type:'varchar',
    })
    operation: string;

    @Column({
        name:'LINE',
        type:'varchar',
    })
    line: string;

    @Column({
        name:'SCHEDULE_NO',
        type:'varchar',
    })
    scheduleNo: string;

    
    // @Column({
    //     name:'status',
    //     type:'varchar',   })
    // status: string;

    @CreateDateColumn({ type: "datetime", default: () => "GETDATE()" })
    created_at: Date;
}
