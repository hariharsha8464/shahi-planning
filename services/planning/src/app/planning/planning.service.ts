import { Injectable } from '@nestjs/common';
import { AppDataSource, projectPlanningDataSource, projectPlanningDataSource01, projectPlanningDataSource23, projectPlanningDataSource3, projectPlanningDataSource54 } from '../app-data-source';
// import axios from 'axios';
import { error } from 'console';
import { LayPlanExcelModel } from './model/lay-plan-excel.model';
import { LayPlanSchedulesEntity } from './entities/Lay-plan-data.entity';
import { LayPlanExcelAdapter } from './adapter/lay-plan-excel.adapter';
import { CommonResponse } from 'libs/libs/shared-models/src/common';
import { OperationReq, UnitReq, WorkStationReq } from 'libs/shared-models';
import { GtsManpowerEntity } from './entities/manpower-entity';
import { DataSource } from 'typeorm';
const https = require('https');
// const axios = require('axios');
// const axiosRetry = require('axios-retry');

const m3Connection = { USER_NAME: 'msrvadm', PASSWORD: 'Shahi@123' };
// axiosRetry(axios, { retries: 5 });
@Injectable()
export class PlanningService {
  constructor(
    private layPlanSchedules: LayPlanExcelAdapter,
  ) { }

  async getDataSourceConnectionForUnit(unitId: any): Promise<DataSource> {
    if (unitId == 7) {
      return projectPlanningDataSource01;
    } else if (unitId == 14) {
      return projectPlanningDataSource23;
    } else if (unitId == 26) {
      return projectPlanningDataSource54;
    } else {
      return projectPlanningDataSource;
    }
  }


  async saveLayPlanExcelData(formData: any): Promise<CommonResponse> {
    const flag = new Set()
    const updatedArray = formData.map((obj) => {

      const updatedObj = {};
      for (const key in obj) {
        const newKey = key.replace(/\s/g, '_').replace(/[\(\)]/g, '').replace(/-/g, '_');
        updatedObj[newKey] = obj[key];
      }
      return updatedObj;
    });
    const convertedData = updatedArray.map((obj) => {
      const updatedObj = {};
      for (const key in obj) {
        const value = obj[key];
        if (value === "") {
          updatedObj[key] = null;
        } else {
          updatedObj[key] = value;
        }
      }

      return updatedObj;
    });
    for (const data of convertedData) {

      const dtoData = new LayPlanExcelModel(data.UNIT, data.OPERATION, data.LINE, String(data.SCHEDULE_NO), data.created_at)
      const convertedExcelEntity: Partial<LayPlanSchedulesEntity> = this.layPlanSchedules.convertDtoToEntity(dtoData);

      const saveExcelEntity: LayPlanSchedulesEntity = await projectPlanningDataSource.getRepository(LayPlanSchedulesEntity).save(convertedExcelEntity);

      const saveExcelDto: LayPlanExcelModel = this.layPlanSchedules.covertEntityToDto(saveExcelEntity);
      if (!saveExcelDto) {
        flag.add(false)
      }
    }
    if (!flag.has(false)) {

      const response = new CommonResponse(true, 0, 'data saved sucessfully')
      return response
    } else {
      throw error
    }
  }

  async getAllPlanData(): Promise<CommonResponse> {
    try {
      const query = await projectPlanningDataSource.getRepository(LayPlanSchedulesEntity).find()
      return new CommonResponse(true, 900, 'data retrieved ', query)
    } catch (error) {
      error
    }
  }

  async getOperationData(Req: OperationReq): Promise<CommonResponse> {
    try {
      let query = `SELECT os.UnitID,sl.SL_Name,os.ItemID,os.Batch,os.UserDate,SUM(os.Quantity) AS TotalQuantity
      FROM [SEPL_Scan].[dbo].[T_Scan_TF_Summary] os
      LEFT JOIN [SEPL_Scan].[dbo].[m_SL] sl ON sl.SL_ID = os.SL_ID `;
      if (Req.operationId) {
        query += `WHERE os.SL_ID = '${Req.operationId}'`
      }
      if (Req.fromDate !== undefined && Req.toDate !== undefined) {
        const formattedFromDate = new Date(Req.fromDate).toISOString().split('T')[0];
        const formattedToDate = new Date(Req.toDate).toISOString().split('T')[0];

        query += ` AND os.UserDate BETWEEN '${Req.fromDate}' AND '${Req.toDate}'`; 
      }
      query += ` GROUP BY os.UnitID, sl.SL_Name, os.ItemID, os.Batch,os.UserDate`;

      const dataSource = await this.getDataSourceConnectionForUnit(Req.unitId);
      if (!dataSource.isInitialized) {
        await dataSource.initialize()
      }

      const data = await (await dataSource).query(query);
      if (data.length) {
        for (const rec of data) {
          const line = rec.SL_Name.split(' ')[0]
          const operations = ["CUT","SEW","FIN"]
          const op = operations.filter((v) => line.includes(v))[0]
          if(Req.unitId === 7 && op === 'FIN'){
            rec.line = 'A1FINO01';
            rec.work_stations = 'A1FINRW1';
          }else{
          const query = `SELECT work_stations, line
            FROM shahi_workstation_lines
            WHERE id > 0 AND line LIKE '%${rec.Batch}' AND unit_id = ${Req.unitId} and work_stations like '%${op}%'`
          const record = await AppDataSource.query(query);
          rec.work_stations = record[0].work_stations
          rec.line = record[0].line
          }
        }
      }


      return new CommonResponse(true, 1111, 'Data Retrieved Successfully', data)
    } catch (err) {
    }

  }

  async getOperationsForDrop(req: UnitReq): Promise<CommonResponse> {
    try {
      const query = `
        SELECT DISTINCT sl.SL_ID, sl.SL_Name
        FROM [SEPL_Scan].[dbo].[m_SL] sl where sl.SL_ID IN ('23','21','22')
      `;
      const dataSource = await this.getDataSourceConnectionForUnit(req.unitId);

      // Check if the connection is established before executing the query
      if (!dataSource.isInitialized) {
        await dataSource.initialize()
      }

      const data = await (await dataSource).query(query);
      return new CommonResponse(true, 444, 'Data retrieved successfully', data);
    } catch (err) {
      throw new Error('Error occurred while fetching data from the database');
    }
  }

  async manpowerUpdate(item: GtsManpowerEntity | GtsManpowerEntity[]): Promise<CommonResponse> {
    const saveToItemListEntity = new Set();

    const manpower = Array.isArray(item) ? item : [item];

    for (const obj of manpower) {
      const manpowerEntity = new GtsManpowerEntity();
      manpowerEntity.id = obj.id;
      manpowerEntity.shift = obj.shift;
      manpowerEntity.lineNumber = obj.lineNumber;
      manpowerEntity.machineNumber = obj.machineNumber;
      manpowerEntity.workStation = obj.workStation;
      manpowerEntity.acceptedPcs = obj.acceptedPcs;
      manpowerEntity.pcRate = obj.pcRate;
      manpowerEntity.rejectedPcs = obj.rejectedPcs;
      manpowerEntity.operators = obj.operators;
      manpowerEntity.kbOperators = obj.kbOperators;
      manpowerEntity.otOperators = obj.otOperators;
      manpowerEntity.posRateOperators = obj.posRateOperators;
      manpowerEntity.unit = obj.unit;
      manpowerEntity.hours = obj.hours;
      manpowerEntity.RPDT = obj.RPDT;
      manpowerEntity.createdAt = obj.createdAt;
      manpowerEntity.createdUser = obj.createdUser
      const save = await AppDataSource.getRepository(GtsManpowerEntity).insert(manpowerEntity);
      if (save) {
        saveToItemListEntity.add(true);
      } else {
        saveToItemListEntity.add(false);
      }
    }
    return saveToItemListEntity.size > 0
      ? new CommonResponse(true, 111, 'Items Created Successfully')
      : new CommonResponse(false, 0, 'No items were created', []);
  }

  async getAllManpowerData(req:UnitReq): Promise<CommonResponse> {
    console.log(req,'req----')
    try {
      const query = ` SELECT u.unit_name AS unit,shift,work_station AS workStation,line_number AS lineNumber,accepted_pcs AS acceptedPcs,pc_rate AS pcRate,rejected_pcs AS rejectedPcs,operators,kb_operators AS kbOperators,
      pos_rate_operators AS posRateOperators,ot_operators AS otOperators,hours,rpdt,m.created_at AS createdAt,created_user AS createdUser FROM manpower_allocation m
      LEFT JOIN shahi_units u ON u.id = m.unit
      where m.unit = '${req.unitId}'`;

      const data = await AppDataSource.query(query);

      return new CommonResponse(true, 1, 'Data retrieved', data)
    } catch (error) {
      throw error;
    }
  }

  async getAllLineNumbersForDropdown(Req: WorkStationReq): Promise<CommonResponse> {
    // console.log(Req, '-------------------')
    try {
      let query = `SELECT work_stations , line FROM shahi_workstation_lines WHERE id > 0 `;

      if (Req.work_stations) {
        query += ` AND work_stations = '${Req.work_stations}'`;
      }
      query += ` GROUP BY line`
      const line = await AppDataSource.query(query);
      return new CommonResponse(true, 11, 'Data retrieved', line);
    } catch (err) {
      throw err;
    }
  }

  async getUnitsForDropdown(): Promise<CommonResponse> {
    try {
      const unitData = await AppDataSource.query(`SELECT id , unit_code as unitCode, unit_name as unitName FROM shahi_units`);
      return new CommonResponse(true, 1, 'Data retrived', unitData)
    } catch (error) {
      throw error;
    }
  }

  async getWorkStationsDrop(Req: UnitReq): Promise<CommonResponse> {
    // console.log(Req, '-------------------')
    try {
      let query = `SELECT  unit_id AS unitId, work_stations FROM shahi_workstation_lines WHERE id > 0`;

      if (Req.unitId) {
        query += ` AND unit_id = '${Req.unitId}'`;
      }
      query += ` group by work_stations`
      // console.log(query)
      const workStations = await AppDataSource.query(query);
      return new CommonResponse(true, 11, 'Data retrieved', workStations);
    } catch (err) {
      throw err;
    }
  }

}
