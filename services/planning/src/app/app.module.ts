import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DepartmentModule } from './masters/department/department.module';
import { DesignationModule } from './masters/designation/designation.module';
import { EmployeeModule } from './masters/employees/employee.module';
import { ProjectModule } from './masters/project/project.module';
import { RoleModule } from './masters/role/role.module';
import { UserModule } from './user/user.module';
import { appConfig } from '../../config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppDataSource, projectPlanningDataSource, projectPlanningDataSource01, projectPlanningDataSource23, projectPlanningDataSource3, projectPlanningDataSource54 } from './app-data-source';
import { DataSource } from 'typeorm';
import { UnitModule } from './masters/branch/unit.module';
import { SectionsEntity } from './masters/department/entity/section.entity';
import { PlanningModule } from './planning/planning.module';

@Module({
  imports: [ DepartmentModule, EmployeeModule, UserModule, DesignationModule, UnitModule,SectionsEntity,PlanningModule],
  controllers: [AppController],
  providers: [AppService,  {
    provide:  DataSource,
    useFactory: async () => {

      await AppDataSource.initialize().then(() => {
        console.log('Data Source 1 has been initialized!');
      }).catch((err) => {
        console.error('Error during Data Source 1 initialization', err);
      });

      // await projectPlanningDataSource23.initialize().then(() => {
      //   console.log('Data Source unit23 has been initialized!');
      // }).catch((err) => {
      //   console.error('Error during Data Source 23 initialization', err);
      // });
      // await projectPlanningDataSource01.initialize().then(() => {
      //   console.log('Data Source unit01 has been initialized!');
      // }).catch((err) => {
      //   console.error('Error during Data Source 01 initialization', err);
      // });
      // await projectPlanningDataSource3.initialize().then(() => {
      //   console.log('Data Source 3 has been initialized!');
      // }).catch((err) => {
      //   console.error('Error during Data Source 3 initialization', err);
      // });
      await projectPlanningDataSource54.initialize().then(() => {
        console.log('Data Source unit54 has been initialized!');
      }).catch((err) => {
        console.error('Error during Data Source 54 initialization', err);
      });
      return [projectPlanningDataSource, projectPlanningDataSource23,projectPlanningDataSource01, projectPlanningDataSource3,projectPlanningDataSource54];
    // const logger = new Logger('DataSourceInitialization'); // Add a logger for better debugging
      // try {
      //   const dataSource = await projectPlanningDataSource.initialize();
      //   logger.log('Data Source has been initialized successfully!');
      //   return dataSource; // Ensure that initialize() returns the correct object with 'name' property
      // } catch (error) {
      //   logger.error(`Error during Data Source initialization: ${error.message}`);
      //   throw error; // Rethrow the error to indicate initialization failure
      // }
    },
  },],
})
export class AppModule { }
