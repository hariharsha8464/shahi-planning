import { appConfig } from "../../../config";
import { DataSource } from "typeorm";
import { UserEntity } from "./entity/user.entity";

export const projectPlanningDataSource = new DataSource({
    type: "mssql",
    host: appConfig.database.host,
    port: appConfig.database.port,
    username: appConfig.database.username,
    password: appConfig.database.password,
    database: appConfig.database.dbName,
    entities: [
        UserEntity
    ],
    name:'a'
})

projectPlanningDataSource.initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err) => {
        console.error("Error during Data Source initialization", err)
    })