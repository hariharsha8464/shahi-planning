import { Body, Controller, HttpException, HttpStatus, Post } from "@nestjs/common";
import { UserMangementService } from "./user.service";
import { AuthResponseModel, LoginDto, UserResponse } from "libs/shared-models";
import { ApplicationExceptionHandler } from 'libs/backend-utils/src/lib/application-exception-handler';
import { CreateUserDto } from "./dto/user.dto";
import { UserRequestDto } from "./dto/user-request.dto";


@Controller("/user-management")
export class UserManagementController {
  constructor(
    private readonly companyService: UserMangementService,
    private readonly applicationExceptionHandler: ApplicationExceptionHandler
  ) { }

 

  @Post('/login')
  async createProject(@Body() dto: any): Promise<AuthResponseModel> {
    try {
      return await this.companyService.login(dto);
    } catch (error) {
      console.log(error,'err')
      return this.applicationExceptionHandler.returnException(AuthResponseModel, error);
    }
  }

  @Post('/getUserById')
  async getUserById(@Body() req:UserRequestDto): Promise<UserResponse> {
    try {
      return await this.companyService.getUserById(req);
    } catch (error) {
      console.log(error,'err')
      return this.applicationExceptionHandler.returnException(UserResponse, error);
    }
  }

  @Post('/getAllUsers')
  async getAllUsers(): Promise<UserResponse> {
    try {
      return await this.companyService.getAllUsers();
    } catch (error) {
      console.log(error,'err')
      return this.applicationExceptionHandler.returnException(UserResponse, error);
    }
  }

  @Post('register')  
  public async register(@Body() createUserDto: any): Promise<any> {    
    const result: 
    any = await this.companyService.register(createUserDto,);
    if (!result.success) {
        throw new HttpException(result.message, HttpStatus.BAD_REQUEST);    
    }
    // console.log(result);
    return result;  
  }

  @Post('/getUsers')
  async getUsers(): Promise<UserResponse> {
    try {
      return await this.companyService.getUsers();
    } catch (error) {
      console.log(error,'err')
      return this.applicationExceptionHandler.returnException(UserResponse, error);
    }
  }
  
}