import * as env from 'dotenv';
export const appConfig = {
    port: {
        port: process.env[`PORT`] ?? 3232
    },
    database: {
        type: process.env[`APP_DB_TYPE`] || 'mssql',
        host: process.env[`APP_DB_HOST`] || '172.25.23.4',
        port: parseInt(process.env.APP_DB_PORT || "3306"),
        username: process.env[`APP_DB_USER`] || 'sa',
        password: process.env[`APP_DB_PASS`] || 'manager@123',
        dbName: process.env[`APP_DB_DBNAME`] || 'Planning',
        poolLimit: parseInt(process.env.APP_DB_POOL_LIMIT || "50"),
    },

}