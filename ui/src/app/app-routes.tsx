import { Route, HashRouter as Router, Routes, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import { ChildProtectionWrapper } from "./common/protected-child-wrapper";
import BasicLayout from "./layout";
import UserForm from "./user/user-form";
import UserFormGrid from "./user/user-grid";
import Login from "./login/login";
import EmployeeGrid from "./master/employee-grid";
import EmployeeForm from "./master/employee-form";
import LayPlanUpload from "./planning/layplan-excel-upload";
import OperationsView from "./planning/operation-data";
import ManpowerUpdateForm from "./planning/manpower-update";
import ManpowerGrid from "./planning/manpower-grid";



const AppRoutes = () => {


    const router = createBrowserRouter(createRoutesFromElements(
        <Route  >
            <Route path='/' key='/' element={
                <ChildProtectionWrapper>
                    <>
                        <BasicLayout />
                    </>
                </ChildProtectionWrapper>
            } >
            </Route>
            <Route path="/login" key='/login' element={<Login />} />
        </Route>
    ))

    return (
        <Router>

            <Routes>
                <Route path="/" element={<ChildProtectionWrapper><BasicLayout /></ChildProtectionWrapper>}>

                    <Route path="/form9" element={<UserForm />} />
                    <Route path="/users" element={<UserFormGrid />} />
                    <Route path="/employee-view" element={<EmployeeGrid />} />
                    <Route path="/employee-form" element={<EmployeeForm employeeData={undefined}

                        isUpdate={false}
                        closeForm={() => { }}
                        updateDetails={(undefined) => { }}
                    />} />
                    <Route path="/planUpload" element={<LayPlanUpload />} />
                    <Route path="/operations-data" element={<OperationsView />} />
                    <Route path="/manpower-update" element={<ManpowerUpdateForm />} />
                    <Route path="/manpower-grid" element={<ManpowerGrid />} />
                </Route>
                <Route path="/login" element={<Login />} />
                
            </Routes>
        </Router>
    );
};

export default AppRoutes;