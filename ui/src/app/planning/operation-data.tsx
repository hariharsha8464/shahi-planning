import { FileExcelFilled, SearchOutlined } from '@ant-design/icons'
import { Button, Card, Col, DatePicker, Empty, Form, Input, Row, Select, Table, message } from 'antd'
import React, { useEffect, useRef, useState } from 'react'
import { PlanningService } from 'libs/shared-services'
import Highlighter from 'react-highlight-words'
import moment from 'moment'
const { Option } = Select;
import { OperationReq, UnitReq } from 'libs/shared-models'
import { Excel } from 'antd-table-saveas-excel'

const OperationsView = () => {
    const service = new PlanningService;
    const [operationData, setOperationData] = useState<any[]>([]);
    const [operationDropData, setOperationDropData] = useState<any[]>([]);
    const [page, setPage] = React.useState(1);
    const [pageSize, setPageSize] = useState<number>(null);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [disable, setDisable] = useState<boolean>(false)
    const [loading, setLoading] = useState<boolean>(false)
    const [selectedUserFromDate, setSelectedUserFromDate] = useState(undefined);
    const [selectedUserToDate, setSelectedUserToDate] = useState(undefined);
    const [filteredData, setFilteredData] = useState<any[]>([])
    const { RangePicker } = DatePicker;
    const [form] = Form.useForm();
    const searchInput = useRef(null);
    const [empty, setEmpty] = useState<boolean>(true)
    const authdata = JSON.parse(localStorage.getItem('userName'))


    useEffect(() => {
        setTimeout(() => setLoading(false), 100000);
        setEmpty(false)
        // getOperationData();
        getOperationsForDrop();
    }, []);


    const getOperationData = () => {
        const Req = new OperationReq()
        const operation = form.getFieldValue('operation');
        Req.fromDate = selectedUserFromDate;
        Req.toDate = selectedUserToDate;
        Req.operationId = operation;
        Req.unitId = authdata.unitId;
        // if (operation) {
        //     filteredData = filteredData.filter(record => record.departmentName === departmentId);
        //     if (filteredData.length === 0) {
        //         AlertMessages.getErrorMessage("No Data Found")
        //     }
        //     setFilteredData(filteredData);
        // }
        setLoading(true);
        service.getOperationData(Req).then(res => {
            setLoading(false); setDisable(false)
            if (res.status) {
                setOperationData(res.data);
                setLoading(false);
            } else {
                if (res.data) {
                    setOperationData([]);
                    setLoading(false);
                    message.success(res.internalMessage);
                } else {
                    message.error(res.internalMessage);
                }
            }
        }).catch(err => {
            setOperationData([]);
            setLoading(false);
            message.error(err);
        })
    }
    const getOperationsForDrop = () => {
        setDisable(true); setLoading(true)
        const req = new UnitReq()
        req.unitId = authdata.unitId;
        console.log(req.unitId)
        service.getOperationsForDrop(req).then((res) => {
            setDisable(false); setLoading(false)
            if (res.status) {
                setOperationDropData(res.data);
                // message.success(res.internalMessage);
            }else {
                message.error(res.internalMessage);
            }
        })
    }

    const onChange = (pagination, filters, sorter, extra) => {
        console.log('params', pagination, filters, sorter, extra);
    }

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };
    const onReset = () => {
        form.resetFields();
        getOperationData();
        setSelectedUserFromDate(undefined);
        setSelectedUserToDate(undefined);
    }

    const getColumnSearchProps = (dataIndex: string) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined />}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button size="small" style={{ width: 90 }}
                    onClick={() => {
                        handleReset(clearFilters)
                        setSearchedColumn(dataIndex);
                        confirm({ closeDropdown: true });
                    }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <SearchOutlined type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                    .toString()
                    .toLowerCase()
                    .includes(value.toLowerCase())
                : false,
        onFilterDropdownVisibleChange: visible => {
            if (visible) { setTimeout(() => searchInput.current.select()); }
        },
        render: text =>
            text ? (
                searchedColumn === dataIndex ? (
                    <Highlighter
                        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                        searchWords={[searchText]}
                        autoEscape
                        textToHighlight={text.toString()}
                    />
                ) : text
            )
                : null
    })
    const UserDateRange = (value) => {
        if (value) {
            const fromDate = value[0].startOf('day').format('YYYY-MM-DD');
            const toDate = value[1].endOf('day').format('YYYY-MM-DD'); 
            setSelectedUserFromDate(fromDate)
            setSelectedUserToDate(toDate)
        }
    }

    const exportExcel = () => {
        const excel = new Excel();
        if (operationData.length > 0) {
            excel
                .addSheet('operation Data')
                .addColumns(data)
                .addDataSource(operationData, { str2num: true });
        }
        excel.saveAs('operationData.xlsx');
    };
    const data = [
        {
            title: 'Unit',
            dataIndex: 'UnitID',
        },
        {
            title: 'Operation',
            dataIndex: 'SL_Name',

        },
        
        {
            title: 'Item',
            dataIndex: 'ItemID',
        },
        
        
        {
            title: 'User Date',
            dataIndex: 'UserDate',
        },
        {
            title: 'Work Station',
            dataIndex: 'work_stations',
        },
        {
            title: 'Line',
            dataIndex: 'line',
        },

        {
            title: 'Quantity',
            dataIndex: 'TotalQuantity',
        },
        
    ];
    const columnsSkelton: any = [
        {
            title: 'S No',
            key: 'sno',
            width: 60,
            responsive: ['sm'],
            render: (text, object, index) => (page - 1) * pageSize + (index + 1)
        },
        {
            title: 'Unit',
            dataIndex: 'UnitID',
            ...getColumnSearchProps('unit'),
        },
       
        {
            title: 'Operation',
            dataIndex: 'SL_Name',
            ...getColumnSearchProps('SL_Name'),

        },
        {
            title: 'Item',
            dataIndex: 'ItemID',
            ...getColumnSearchProps('ItemID'),
        },
        
        {
            title: 'User Date',
            dataIndex: 'UserDate',
            render: (text, record) => {
                const UserDate = record.UserDate;
                if (UserDate) {
                    return moment(UserDate).format('YYYY-MM-DD');
                } else {
                    return '-';
                }
            }
        },
        {
            title: 'Work Station',
            dataIndex: 'work_stations',
        },
        {
            title: 'Line',
            dataIndex: 'line',
        },
        {
            title: 'Quantity',
            dataIndex: 'TotalQuantity',
            align: 'right',
            fixed: 'right'
        },

    ]
    return (
        <div>
            <Card title={<span style={{ color: 'white' }}>Operation Details</span>
            }
                style={{ textAlign: 'center' }} headStyle={{ backgroundColor: '#7d33a2', border: 0 }}
                extra={
                    <Button
                        type="default"
                        style={{ color: 'green' }}
                        onClick={exportExcel}
                        icon={<FileExcelFilled />}
                    >
                        GET EXCEL
                    </Button>
                }
            >
                <br></br>
                <Form form={form} layout={'vertical'} >
                    <Row gutter={40}>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 5 }} lg={{ span: 6 }} xl={{ span: 6 }}>
                            <Form.Item
                                name="operation"
                                label=" Operation"
                                rules={[
                                    {
                                        required: true, message: 'Select Operation',
                                    },
                                ]}
                            >
                                <Select
                                    loading={operationDropData.length == 0}
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Operation"
                                    allowClear
                                    style={{ width: '100%' }}
                                >
                                    {operationDropData.map(dropData => {
                                        return <Option key={dropData.SL_ID} value={dropData.SL_ID}>{dropData.SL_Name}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }}>
                            <Form.Item name="fromDate"
                                label="User Date"
                                rules={[
                                    {
                                        required: false,
                                        message: "Missing Created Date"
                                    },
                                ]}>
                                <RangePicker onChange={UserDateRange} />
                            </Form.Item>
                        </Col>
                        <Col style={{ marginTop: '30px' }}>
                            <Button style={{ marginRight: '4px' }} loading={loading} onClick={getOperationData}
                            >Get Operation</Button>
                        </Col>
                        <Col style={{ marginTop: '30px' }}>
                            <Button type="primary" htmlType="submit" onClick={onReset}>
                                Reset
                            </Button>
                        </Col>
                    </Row>
                </Form>

                <br></br>
                {operationData?.length > 0 ?
                    <>
                        <Table
                            // rowKey={record => record.employeeId}
                            columns={columnsSkelton}
                            dataSource={operationData}
                            scroll={{ y: 400 }}
                            sticky={true}
                            pagination={{
                                onChange(current, pageSize) {
                                    setPage(current);
                                    setPageSize(pageSize)
                                }
                            }}
                            onChange={onChange}
                            bordered
                        />
                    </> : <Empty description={empty ? 'No data found' : 'Select inputs and click search to get data'}></Empty>}


            </Card>
        </div>
    )
}

export default OperationsView