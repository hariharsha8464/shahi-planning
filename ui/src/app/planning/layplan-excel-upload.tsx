import React, { useEffect, useRef, useState } from 'react';
import { Form, Input, Button, Card, Space, message, Row, Col } from 'antd';
import * as XLSX from 'xlsx';
import Table, { ColumnProps } from 'antd/lib/table';
import { ExportOutlined, ImportOutlined, SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import Highlighter from 'react-highlight-words';
import { PlanningService } from 'libs/shared-services';
import { CommonResponse } from 'libs/libs/shared-models/src/common';

interface SearchColumnsDataType {

}

const LayPlanUpload = () => {
    // type DataIndex = keyof SearchColumnsDataType;
  const [formData, setFormData] = useState<any[]>([]);
  const [tableData,setTableData] = useState<any[]>([]);
  const [excelData,setExcelData] = useState<any>([]);
  const [upload, setUpload] = useState<any>([]);
  const [searchText, setSearchText] = useState(''); 
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);
  const service = new PlanningService();

  useEffect(()=>{
     getAllPlanData();
  },[])


  const authdata = JSON.parse(localStorage.getItem('userName'))
    console.log(authdata, 'authData-----')
   

  const handleFileChange = (e:any) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = (e:any) => {
      const data = new Uint8Array(e.target.result);
      const workbook = XLSX.read(data, { type: 'array' });
      const firstSheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[firstSheetName];
      const sheetData = XLSX.utils.sheet_to_json(worksheet);
      console.log(sheetData)
      setFormData(sheetData);
    };
    reader.readAsArrayBuffer(file);
  };

  const saveExcelData=(values:CommonResponse[])=>{
    service.saveLayPlanExcelData(formData).then((res)=>{
      if(res.status){
          message.success(res.internalMessage);
        //   excelImportTabledata()
      }else{
          //setTableData(formData)
          message.error(res.internalMessage);
      }
    }).catch(err => {
      message.error(err.message);
  });
}

const getAllPlanData = () => {
  service.getAllPlanData().then((res)=>{
    if(res.status){
      setExcelData(res.data)
      message.success('data retrieved')
    }else{
      message.error('some thing went to wrong')
    }
  })
}


const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
};

const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
};

const getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div style={{ padding: 8 }}>
            <Input
                ref={searchInput}
                placeholder={`Search ${dataIndex}`}
                value={selectedKeys[0]}
                onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
                type="primary"
                onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                icon={<SearchOutlined />}
                size="small"
                style={{ width: 90, marginRight: 8 }}
            >
                Search
            </Button>
            <Button size="small" style={{ width: 90 }}
                onClick={() => {
                    handleReset(clearFilters)
                    setSearchedColumn(dataIndex);
                    confirm({ closeDropdown: true });
                }}>
                Reset
            </Button>
        </div>
    ),
    filterIcon: filtered => (
        <SearchOutlined type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
        record[dataIndex]
            ? record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase())
            : false,
    onFilterDropdownVisibleChange: visible => {
        if (visible) { setTimeout(() => searchInput.current.select()); }
    },
    render: text =>
        text ? (
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : text
        )
            : null
})

const columnsSkelton: any = [
  // {
  //     title: 'S No',
  //     key: 'sno',
  //     width: '70px',
  //     align: 'center',
  //     render: (text, object, index) => (0) + (index + 1)
  // },
  {
    title: 'Unit',
    dataIndex: 'unit',
    align: 'center',
    fixed:'left',
    ...getColumnSearchProps('unit'),
    // render: (value: string) => (
    //   <div style={{ textAlign: 'center' }}>{value}</div>
    // ),
  },
  {
    title: 'Operation',
    dataIndex: 'operation',
    align: 'center',
    fixed:'left',
    ...getColumnSearchProps('operation'),
    // render: (value: string) => (
    //   <div style={{ textAlign: 'center' }}>{value}</div>
    // ),
  },
  {
    title: 'Line',
    dataIndex: 'line',
    align: 'center',
    fixed:'left',
    ...getColumnSearchProps('operation'),
    // render: (value: string) => (
    //   <div style={{ textAlign: 'center' }}>{value}</div>
    // ),
  },
  {
    title: 'Schedule No',
    dataIndex: 'scheduleNo',
    align: 'center',
    fixed:'left',
    ...getColumnSearchProps('scheduleNo'),
    render: (value: string) => (
      <div style={{ textAlign: 'center' }}>{value}</div>
    ),
  },

  {
    title: 'CREATED DATE',
    dataIndex: 'created_at',
    align: 'center',
    width: 130,
    render: (text, record) => {
      return moment(record.created_at).format('YYYY-MM-DD HH:mm:ss') || '-';
    }
  },

];



  return (
    <>
      <Form onFinish={saveExcelData}>
        <Row gutter={24}>
          <Col>
            <Form.Item label="File In xlsx">
              <input type="file" onChange={handleFileChange} />
            </Form.Item>
            </Col>
            <Col>
            <Form.Item>
              <Button icon={<ImportOutlined />} type="primary" htmlType="submit">
                File Upload 
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Card title="Uploaded Data" >
        <Table
          rowKey={(record) => record.excelImportId}
          columns={columnsSkelton}
          dataSource={excelData}
          scroll={{ x:1500, y: 300 }}
          rowClassName="custom-cell"
        />
      </Card>
    </>
  );
};

export default LayPlanUpload;