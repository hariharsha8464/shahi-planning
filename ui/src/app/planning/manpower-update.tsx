import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Select, Card, Row, Col, message, Table } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { FileExcelFilled, SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import { Excel } from 'antd-table-saveas-excel';
import { PlanningService } from 'libs/shared-services';
import { ManPowerDto, ShiftsEnum } from 'libs/shared-models';

const { TextArea } = Input;

/* eslint-disable-next-line */
export interface ManpowerFormProps {
}

export function ManpowerUpdateForm(props: ManpowerFormProps) {
    const [form] = Form.useForm();
    const [disable, setDisable] = useState<boolean>(false);
    const [manpowerData, setManpowerData] = useState<any>([]);
    const [lineData, setLineData] = useState<any>([]);
    const [workStationData, setWorkStationData] = useState<any>([]);
    const [unitData, setUnitData] = useState<any>([]);
    const [machineHidden, setMachineHidden] = useState<boolean>(true);
    const authdata = JSON.parse(localStorage.getItem('userName'))

    const service = new PlanningService();
    const navigate = useNavigate();
    const { Option } = Select;

    useEffect(() => {
        getUnitsForDropdown();
        getAllManpowerData();
        getWorkStationsDrop();
        form.setFieldsValue({ unitId: Number(localStorage.getItem('unitId')) })
        form.setFieldsValue({createdUser:authdata.userName})
        
    }, [])

    const getAllLineNumbersForDropdown = () => {
        const WorkStationValue = form.getFieldValue('workStation');
        const Req = { work_stations: WorkStationValue };
        console.log(Req)
        service.getAllLineNumbersForDropdown(Req).then(res => {
            if (res.status) {
                setLineData(res.data);
            } else {
                if (res.data) {
                    setLineData([]);
                    message.success("Data retrieve successfully");
                } else {
                    message.success("Data retrieve successfully");
                }
            }
        }).catch(err => {
            message.error("Data not found");
            setLineData([]);
        })

    }

    const getWorkStationsDrop = () => {
        const unitValue = authdata.unitId;
        const Req = { unitId: unitValue };
        console.log(Req)
        service.getWorkStationsDrop(Req)
            .then(res => {
                if (res.status) {
                    setWorkStationData(res.data);
                } else {
                    if (res.data) {
                        setWorkStationData([]);
                        message.success("Data retrieved successfully");
                    } else {
                        message.error("No Data");
                    }
                }
            })
            .catch(err => {
                message.error("Data not found");
                setWorkStationData([]);
            });
    };

    const getAllManpowerData = () => {
        const unitValue = authdata.unitId;
        const req = { unitId: unitValue };
        service.getAllManpowerData(req).then(res => {
            if (res.status) {
                setManpowerData(res.data);
            } else {
                if (res.data) {
                    setManpowerData([]);
                    message.success("Data retrieve successfully");
                } else {
                    message.success("Data retrieve successfully");
                }
            }
        }).catch(err => {
            message.error("Data not found");
            setManpowerData([]);
        })

    }

    const getUnitsForDropdown = () => {
        service.getUnitsForDropdown().then(res => {
            if (res.status) {
                setUnitData(res.data);
            } else {
                if (res.data) {
                    setUnitData([]);
                    message.success("Data retrieve successfully");
                } else {
                    message.success("Data retrieve successfully");
                }
            }
        }).catch(err => {
            message.error("Data not found");
            setUnitData([]);
        })

    }

    const manpowerUpdate = (item: ManPowerDto) => {
        setDisable(true)
        service.manpowerUpdate(item).then(res => {
            setDisable(false)
            if (res.status) {
                console.log(res.status)
                message.success('Man Power Updated Successfully');
                onReset();
                navigate('/manpower-grid')
                getAllManpowerData();
            } else {
                message.error(res.internalMessage);
            }
        }).catch(err => {
            setDisable(false)
            message.error(err.message);
        })
    }

    const onReset = () => {
        form.resetFields();
    };

    const saveData = (values: ManPowerDto) => {
        setDisable(false)
        manpowerUpdate(values);
    };


    return (
        <Card
            title="Manpower Update"
            headStyle={{ backgroundColor: '#7d33a2', color: 'white' }} extra={<Link to='/manpower-grid' ><span style={{ color: 'white' }} ><Button className='panel_button' >View </Button> </span></Link>}
        >
            <Form
                form={form}
                //   initialValues={props.certificationData}
                name="control-hooks"
                onFinish={saveData}
                layout="vertical"
            >

                <Row>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 3 }} style={{ margin: '1%' }}
                    >
                        <Form.Item name="unit" label="Unit"
                            initialValue={authdata.unitId}
                            rules={[
                                {
                                    required: true,
                                    message: 'Enter Unit ',
                                },
                            ]}
                        >
                            <Select
                                placeholder='Select Unit'
                                allowClear
                                showSearch
                                // onChange={getWorkStationsDrop}
                                filterOption={(input, option) => {
                                    const label = option.label?.toString().toLowerCase();
                                    const value = option.value?.toString().toLowerCase();
                                    const searchInput = input.toLowerCase();
                                    return label?.includes(searchInput) || value?.includes(searchInput);
                                }}
                                disabled
                            >
                                {unitData.map((i) => (
                                    <Select.Option key={i.id} value={i.id}>
                                        {i.unitName}
                                    </Select.Option>
                                ))}

                            </Select>
                        </Form.Item>
                    </Col>

                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 3 }} style={{ margin: '1%' }}
                    >
                        <Form.Item name="workStation" label="Workstation"
                            rules={[
                                {
                                    required: true,
                                    message: 'select Workstation ',
                                },
                            ]}
                        >
                            <Select
                                placeholder='Select Workstation'
                                allowClear
                                showSearch
                                onChange={getAllLineNumbersForDropdown}
                                filterOption={(input, option) => {
                                    const label = option.label?.toString().toLowerCase();
                                    const value = option.value?.toString().toLowerCase();
                                    const searchInput = input.toLowerCase();
                                    return label?.includes(searchInput) || value?.includes(searchInput);
                                }}
                            >
                                {workStationData.map((i) => (
                                    <Select.Option key={i.work_stations} value={i.work_stations}>
                                        {i.work_stations}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>

                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 3 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="lineNumber"
                            label="Line Number"
                            rules={[
                                {
                                    required: true,
                                    message: 'Enter Line',
                                },
                            ]}
                        >
                            <Select
                                placeholder='Select LINE'
                                allowClear
                                showSearch
                                filterOption={(input, option) => {
                                    const label = option.label?.toString().toLowerCase();
                                    const value = option.value?.toString().toLowerCase();
                                    const searchInput = input.toLowerCase();

                                    // Check if either label or value contains the search input
                                    return label?.includes(searchInput) || value?.includes(searchInput);
                                }}
                            >
                                {lineData.map((i) => (
                                    <Select.Option key={i.line} value={i.line}>
                                        {i.line}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>

                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item name="shift" label="Shift"
                            rules={[
                                {
                                    required: true,
                                    message: 'Enter Shift ',
                                },
                            ]}
                        >
                            <Select
                                placeholder='Select Shift'
                                allowClear
                                showSearch
                                filterOption={(input, option) => {
                                    const label = option.label?.toString().toLowerCase();
                                    const value = option.value?.toString().toLowerCase();
                                    const searchInput = input.toLowerCase();

                                    // Check if either label or value contains the search input
                                    return label?.includes(searchInput) || value?.includes(searchInput);
                                }}
                            >
                                {Object.values(ShiftsEnum).map((value) => {
                                    return (
                                        <Select.Option key={value} value={value}>
                                            {value}
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="acceptedPcs"
                            label="Accepted(Pieces)"

                            rules={[
                                {
                                    // max: 3,
                                    required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="pcRate"
                            label="PC Rate(Pieces)"

                            rules={[
                                {
                                    // max: 3,
                                    // required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="rejectedPcs"
                            label="Rejected(Pieces)"

                            rules={[
                                {
                                    // max: 3,
                                    // required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="operators"
                            label="Operators"

                            rules={[
                                {
                                    // max: 3,
                                    required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>

                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="kbOperators"
                            label="KB-Operators"

                            rules={[
                                {
                                    // max: 3,
                                    required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="posRateOperators"
                            label="POS-Rate Operators"

                            rules={[
                                {
                                    // max: 3,
                                    // required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="otOperators"
                            label="OT-Operators"

                            rules={[
                                {
                                    // max: 3,
                                    // required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="hours"
                            label="Hours (In Minutes)"

                            rules={[
                                {
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                        <Form.Item
                            name="RPDT"
                            label="RPDT(YYYYMMDD)"

                            rules={[
                                {
                                    max: 7,
                                    required: true,
                                    pattern: /^[0-9]+$/,
                                    message: `It Should Contain Only Numbers & Please Enter Above Formate`,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 4 }} style={{ margin: '1%' }}
                    >
                    <Form.Item style={{ display: "none" }} name="createdUser"  >
                    </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" disabled={disable} htmlType="submit">
                            Submit
                        </Button>
                        {
                            <Button
                                htmlType="button"
                                style={{ margin: '0 14px' }}
                                onClick={onReset}
                            >
                                Reset
                            </Button>
                        }
                    </Col>
                </Row>
            </Form>
        </Card>
    );
}

export default ManpowerUpdateForm;
