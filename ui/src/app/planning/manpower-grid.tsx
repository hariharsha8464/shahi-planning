import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, Button, Select, Card, Row, Col, message, Table } from 'antd';
import { PlanningService } from 'libs/shared-services';
import { Excel } from 'antd-table-saveas-excel';
import Highlighter from 'react-highlight-words';
import { FileExcelFilled, SearchOutlined } from '@ant-design/icons';
import moment from 'moment';


export interface ManpowerGridProps {
}

export function ManpowerGrid(props: ManpowerGridProps) {
    const [manpowerData, setManpowerData] = useState<any>([]);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);
    const service = new PlanningService();
    const authdata = JSON.parse(localStorage.getItem('userName'))

    useEffect(() => {
        getAllManpowerData();
         Number(localStorage.getItem('unitId')) 
    }, [])

    const getAllManpowerData = () => {
        const unitValue = authdata.unitId;
        const req = { unitId: unitValue };
        console.log(req)
        service.getAllManpowerData(req).then(res => {
            if (res.status) {
                setManpowerData(res.data);
            } else {
                if (res.data) {
                    setManpowerData([]);
                    message.success("Data retrieve successfully");
                } else {
                    message.success("Data retrieve successfully");
                }
            }
        }).catch(err => {
            message.error("Data not found");
            setManpowerData([]);
        })

    }


    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = (dataIndex: string) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined />}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button size="small" style={{ width: 90 }}
                    onClick={() => {
                        handleReset(clearFilters)
                        setSearchedColumn(dataIndex);
                        confirm({ closeDropdown: true });
                    }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <SearchOutlined type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                    .toString()
                    .toLowerCase()
                    .includes(value.toLowerCase())
                : false,
        onFilterDropdownVisibleChange: visible => {
            if (visible) { setTimeout(() => searchInput.current.select()); }
        },
        render: text =>
            text ? (
                searchedColumn === dataIndex ? (
                    <Highlighter
                        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                        searchWords={[searchText]}
                        autoEscape
                        textToHighlight={text.toString()}
                    />
                ) : text
            )
                : null
    })

    const exportExcel = () => {
        const excel = new Excel();
        if (manpowerData.length > 0) {
            excel
                .addSheet('Manpower Data')
                .addColumns(data1)
                .addDataSource(manpowerData, { str2num: true });
        }
        excel.saveAs('ManpowerData.xlsx');
    };
    const data1 = [
        // {
        //     title: 'Unit',
        //     dataIndex: 'unit',
        // },
        {
            title: 'WorkCenter',
            dataIndex: 'workStation',
        },
        {
            title: 'Line',
            dataIndex: 'lineNumber',
        },
        // {
        //     title: 'Shift',
        //     dataIndex: 'shift',
        // },
        {
            title: 'Accepted(Pieces)',
            dataIndex: 'acceptedPcs',
        },
        // {
        //     title: 'PC Rate',
        //     dataIndex: 'pcRate',
        // },
        // {
        //     title: 'Rejected Pcs',
        //     dataIndex: 'rejectedPcs'
        // },
        {
            title: 'Operators',
            dataIndex: 'operators',
        },

        {
            title: 'KB Operators',
            dataIndex: 'kbOperators',
        },
        // {
        //     title: 'Pos RateOperators',
        //     dataIndex: 'posRateOperators',
        // },
        // {
        //     title: 'Ot Operators',
        //     dataIndex: 'otOperators',
        // },
        // {
        //     title: 'Hours',
        //     dataIndex: 'hours',
        // },
        // {
        //     title: 'RPDT',
        //     dataIndex: 'RPDT',
        // },
    ];

    const columnsSkelton: any = [
        {
            title: 'S No',
            key: 'sno',
            width: '70px',
            align: 'center',
            render: (text, object, index) => (0) + (index + 1)
        },
        {
            title: 'Unit',
            dataIndex: 'unit',
            align: 'center',
        },
        {
            title: 'Work Station',
            dataIndex: 'workStation',
            align: 'center',
        },
        {
            title: 'Line',
            dataIndex: 'lineNumber',
            align: 'center',
        },
        {
            title: 'Shift',
            dataIndex: 'shift',
            align: 'center',
        },
        {
            title: 'Accepted (Pieces)',
            dataIndex: 'acceptedPcs',
            align: 'right',
        },
        {
            title: 'PC Rate',
            dataIndex: 'pcRate',
            align: 'right',
        },
        {
            title: 'Rejected Pcs',
            dataIndex: 'rejectedPcs',
            align: 'right',
        },
        {
            title: 'Operators',
            dataIndex: 'operators',
            align: 'center',
        },
        {
            title: 'KB Operators',
            dataIndex: 'kbOperators',
            align: 'center',
        },
        {
            title: 'POS Rate Operators',
            dataIndex: 'posRateOperators',
            align: 'center',
        },
        {
            title: 'OT Operators',
            dataIndex: 'otOperators',
            align: 'center',
        },
        {
            title: 'Hours',
            dataIndex: 'hours',
            align: 'center',
        },
        {
            title: 'RPDT',
            dataIndex: 'rpdt',
            align: 'center',
        },
        {
            title: 'Create User',
            dataIndex: 'createdUser',
            align: 'center',
        },
        {
            title: 'Create At',
            dataIndex: 'createdAt',
            render: (text, record) => {
                const createdAt = record.createdAt;
                if (createdAt) {
                    return moment(createdAt).format('YYYY-MM-DD');
                } else {
                    return '-';
                }
            }
        }
    ];
    return (
        <Card title="Manpower Data"
            extra={
                <Button
                    type="default"
                    style={{ color: 'green' }}
                    onClick={exportExcel}
                    icon={<FileExcelFilled />}
                >
                    GET EXCEL
                </Button>
            }>
            <Table
                rowKey={(record) => record.excelImportId}
                columns={columnsSkelton}
                dataSource={manpowerData}
                scroll={{ x: 1700, y: 300 }}
                rowClassName="custom-cell"
            />
        </Card>
    )


} export default ManpowerGrid