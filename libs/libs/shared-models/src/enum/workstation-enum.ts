export enum WorkstationEnum{
    SEWING = 'SEWING',
    WASHING = 'WASHING',
    FINISHING = 'FINISHING',
}