export class OperationReq{
    operationId:number;
    fromDate?:string;
    toDate?:string;
    itemNo?:string;
    unitId?:number;
}