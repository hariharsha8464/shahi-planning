export class ManPowerDto {
    id:number;
    unit: number;
    shift: string;
    workStation:string;
    lineNumber: string;
    machineNumber: string;
    acceptedPcs:number;
    pcRate:number;
    rejectedPcs:number;
    operators: number;
    kbOperators: number;
    posRateOperators: number;
    otOperators: number;
    hours: number;
    RPDT: number;
    createdAt:Date
    createdUser:string;
}