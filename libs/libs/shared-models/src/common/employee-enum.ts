export enum EmployeeRolesEnum {
    ADMIN = 'ADMIN',
    ASSISTANT_SALES_MANAGER = 'ASSISTANT SALES MANAGER',//head office sales to make saleorders
    HO_SALES_MANAGER = 'SALES MANAGER',//head office sales to make saleorders
    SR_SALES_MANAGER = 'SENIOR SALES MANAGER',//head office sales to make saleorder
    SALES = 'SALES'
}