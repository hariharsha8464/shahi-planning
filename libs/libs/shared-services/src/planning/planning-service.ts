
import { CommonResponse } from "libs/libs/shared-models/src/common";
import { CommonAxiosServicePms } from "../common-axios-service-prs";
import { OperationReq, UnitReq, WorkStationReq } from "libs/shared-models";

export class PlanningService extends CommonAxiosServicePms {
    private PlanningController = "/planning";

    async saveLayPlanExcelData(payload: any): Promise<CommonResponse> {
        return this.axiosPostCall(this.PlanningController + "/saveLayPlanExcelData", payload)
    }
    async getAllPlanData ():Promise<CommonResponse>{
        return this.axiosGetCall(this.PlanningController + "/getAllPlanData")
    }
    async getOperationData (Req : OperationReq):Promise<CommonResponse>{
        return this.axiosPostCall(this.PlanningController + "/getOperationData",Req)
    }
    async getOperationsForDrop (req:UnitReq):Promise<CommonResponse>{
        return this.axiosPostCall(this.PlanningController + "/getOperationsForDrop",req)
    }
    async manpowerUpdate(item: any): Promise<CommonResponse> {
        return this.axiosPostCall(this.PlanningController + "/manpowerUpdate",item)
    }
    async getAllManpowerData(req:UnitReq): Promise<CommonResponse> {
        return this.axiosPostCall(this.PlanningController + "/getAllManpowerData",req)
    }
    async getAllLineNumbersForDropdown(Req:WorkStationReq): Promise<CommonResponse> {
        console.log('sharwd',Req)
        return this.axiosPostCall(this.PlanningController + "/getAllLineNumbersForDropdown",Req)
    }
    async getWorkStationsDrop(Req: UnitReq): Promise<CommonResponse> {
        return this.axiosPostCall(this.PlanningController + "/getWorkStationsDrop",Req)
    }
    async getUnitsForDropdown(): Promise<CommonResponse> {
        return this.axiosGetCall(this.PlanningController + "/getUnitsForDropdown")
    }
}