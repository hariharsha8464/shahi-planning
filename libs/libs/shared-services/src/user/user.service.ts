import { AuthResponseModel,CreateUserDto,LoginDto, UserModel } from 'libs/shared-models'
import { CommonAxiosServicePms } from "../common-axios-service-prs";

export class UserManagementServices extends CommonAxiosServicePms {
    private userManagementController = '/user-management';


    async login(dto: LoginDto): Promise<AuthResponseModel> {
        return await this.axiosPostCall(this.userManagementController + '/login', dto)
    }
    async getAllUsers(): Promise<any> {
        return await this.axiosPostCall(this.userManagementController + '/getAllUsers')
    }

    async register(dto:CreateUserDto): Promise<any> {
        return await this.axiosPostCall(this.userManagementController + '/register',dto)
    }

    async getUsers(): Promise<any> {
        return await this.axiosPostCall(this.userManagementController + '/getUsers')
    }
}